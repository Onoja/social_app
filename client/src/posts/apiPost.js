export const create = (userId, token, post) => {
  console.log(post, " post photo")
  return fetch(`/api/post/new/${userId}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`
    },
    body: post
  })
    .then(response => response.json())
    .catch(err => console.log(err));
}

export const list = () => {
  return fetch(`/api/posts`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  })
    .then(response => response.json());
}

export const singlePost = (postId) => {
  return fetch(`/api/post/${postId}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  })
    .then(response => response.json());
}

export const listByUser = (userId, token) => {
  return fetch(`/api/post/by/${userId }`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  })
    .then(response => response.json());
}

export const remove = (postId, token) => {
  return fetch(`/api/post/${postId}`, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  })
    .then(response => response.json());
}

export const updatePost = (postId, token, post) => {
  return fetch(`/api/post/${postId}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`
    },
    body: post
  })
    .then(response => response.json())
    .catch(err => console.log(err));
}