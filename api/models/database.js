const mongoose = require("mongoose");
require("dotenv").config();

module.exports = () => {
  mongoose.Promise = global.Promise;
  mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@ds263436.mlab.com:63436/${process.env.DB_NAME}`, { useNewUrlParser: true })
    .then(() => console.log("Connection to database esterblished"))
    .catch(err => console.log(err.message));
}
// mongodb://<dbuser>:<dbpassword>social-app